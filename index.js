var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

function player(id, x, y, username, direction){
    this.id = id;
    this.x = x;
    this.y = y;
    this.username = username;
    this.direction = direction;
    var location;
}

function enemy(id, classname, x, y, alive, location, movementSpeed, maxHealth){
    this.id = id;
    this.classname = classname;
    this.x = x;
    this.y = y;
    this.alive = alive;
    this.location = location;
    this.isInCombat = false;
    this.direction = Math.floor(Math.random() * 4) + 1;
    this.movementSpeed = movementSpeed;
    this.combatMovementSpeed = movementSpeed * 5;
    this.currentHealth = maxHealth;
    this.maxHealth = maxHealth;
    this.target = null;
}
function skillPackage(skillName, sender, target) {
    this.skillName = skillName;
    this.sender = sender;
    this.target = target;
}

// Variables begin //
var running = true;
var players = [];
var clients = [];
var enemies = [
    // TUTORIALMAP01
    new enemy("0", "Frog", 1200, 500, true, "TutorialMap01", 10, 100),
    new enemy("1", "Frog", 1400, 222, true, "TutorialMap01", 10, 100),
    new enemy("2", "Frog", 200, 300, true, "TutorialMap01", 10, 100),
    new enemy("3", "Frog", 400, 900, true, "TutorialMap01", 10, 100),
    new enemy("4", "Frog", 350, 400, true, "TutorialMap01", 10, 100),
    // TUTORIALCAVE01
    new enemy("5", "Frog", 1400, 380, true, "TutorialCave01", 10, 100),
    new enemy("6", "Frog", 500, 120, true, "TutorialCave01", 10, 100),
    new enemy("7", "Frog", 700, 250, true, "TutorialCave01", 10, 100),
    new enemy("8", "Frog", 200, 350, true, "TutorialCave01", 10, 100),
    new enemy("9", "Frog", 1100, 400, true, "TutorialCave01", 10, 100),
    new enemy("10", "Frog", 1200, 100, true, "TutorialCave01", 10, 100),
    new enemy("11", "Frog", 1300, 300, true, "TutorialCave01", 10, 100),
    // BATCAVE
    new enemy("12", "Bat", 1400, 380, true, "BatCave", 10, 500),
    new enemy("13", "Bat", 500, 120, true, "BatCave", 10, 500),
    new enemy("14", "Bat", 700, 250, true, "BatCave", 10, 500),
    new enemy("15", "Bat", 200, 350, true, "BatCave", 10, 500),
    new enemy("16", "Bat", 1100, 400, true, "BatCave", 10, 500),
    new enemy("17", "Bat", 1200, 100, true, "BatCave", 10, 500),
    new enemy("18", "Bat", 1300, 300, true, "BatCave", 10, 500)
];
// Variables end //

server.listen(8080, function(){
	console.log("Server is now running...");
});

io.on('connection', function(socket){
    playerConnected(socket);
    playerSetUserName(socket);
    playerSetLocation(socket);
    playerRequestedLocation(socket);
    playerMoved(socket);
    playerDisconnected(socket);
    usedSkill(socket);
    playerEquippedItem(socket);
    playerUnEquippedItem(socket);
    enemyDied(socket);
    enemyDamaged(socket);
});
setInterval(moveEnemies, 500);
setInterval(changeEnemiesDirection, 3000);
//setInterval(respawnEnemy, 30000);
setInterval(respawnEnemy, 5000);

function getPlayerById(id) {
    for(var i = 0; i < players.length; i++) {
        if(players[i].id == id) {
            return players[i];
        }
    }
}

// SERVER FUNCTIONS BEGIN //
function playerConnected(socket){
    // On connection, get information about players
        // and emit that you joined1
    	console.log("Player Connected.");
    	socket.emit('socketID', { id: socket.id });
        socket.broadcast.emit('newPlayer', { id: socket.id });

        // Add yourself to the player list
            players.push(new player(socket.id, 0, 0, "", 0));
            clients.push(socket);
}
function playerSetUserName(socket){
    // If the player set username
        socket.on('setUserName', function(data){
            data.id = socket.id;
            socket.broadcast.emit('setUserName', data);

            for(var i = 0; i < players.length; i++){
                if(players[i].id == data.id){
                    players[i].username = data.username;
                }
            }
        });
}
function playerSetLocation(socket){
    socket.on('setLocation', function(data){
        data.id = socket.id;
        socket.broadcast.emit('setLocation', data);
        changeRoom(socket, data);
        sendOutPlayers(socket, data);
        sendOutEnemies(socket, data);
    });
}
function playerRequestedLocation(socket){
    socket.on('requestLocation', function(data){
        for(var i = 0; i < players.length; i++) {
            if(players[i].id == data.id) {
                socket.emit('getRequestedPlayerLocation', players[i]);
                break;
            }
        }
        
    });
}
function playerMoved(socket){
    // If the player moved
        socket.on('playerMoved', function(data){
            data.id = socket.id;
            io.sockets.in(data.location).emit('playerMoved', data);

            for(var i = 0; i < players.length; i++){
                if(players[i].id == data.id){
                    players[i].x = data.x;
                    players[i].y = data.y;
                }
            }
        });
}
function playerDisconnected(socket){
    // Emit if you disconnect
    	socket.on('disconnect', function(){
    		console.log("Player Disconnected.");
    		socket.broadcast.emit('playerDisconnected', { id: socket.id });
    		for(var i = 0; i < players.length; i++){
    		    if(players[i].id == socket.id){
                    players.splice(i, 1);
                    clients.splice(i, 1);                    
    		    }
    		}
    	});
}
function usedSkill(socket) {
    socket.on('usedSkill', function(data){
        var skillName = data.skillName;
        var sender = data.sender;
        var target = data.target;
        var skill = new skillPackage(skillName, sender, target);
        var location = data.location;
        socket.in(location).broadcast.emit('usedSkill', skill);
    });
}
function playerEquippedItem(socket) {
    socket.on('equippedItem', function(data) {
        socket.in(getPlayerById(data.id).location).broadcast.emit('equippedItem', data);
    });
}
function playerUnEquippedItem(socket) {
    socket.on('unEquippedItem', function(data) {
        socket.in(getPlayerById(data.id).location).broadcast.emit('unEquippedItem', data);
    });
}
function enemyDied(socket) {
    socket.on('enemyDied', function(data){
        for(var i = 0; i < enemies.length; i++) {
            if(enemies[i].id == data.id) {
                enemies[i].alive = false;
                enemies[i].isInCombat = false;
                enemies[i].target = null;
            }
        }
    });
}
// SERVER FUNCTIONS END //

function sendOutPlayers(socket, data) {
    var tempPlayers = [];
    for(var i = 0; i < players.length; i++){
        if(players[i].id == data.id){
            players[i].location = data.location;
        }
        else {
            if(players[i].location == data.location)  {
                tempPlayers.push(players[i]);
            }
        }
    }
    socket.emit('getPlayers', tempPlayers);
}
function sendOutEnemies(socket, data){
    var tempEnemies = [];
    for(var i = 0; i < enemies.length; i++){
        if(enemies[i].location == data.location 
            && enemies[i].alive){
            tempEnemies.push(enemies[i]);
        }
    }
    socket.emit('getEnemies', tempEnemies);
}
function moveEnemy(enemy){
    tempClients = [];
    for(var i = 0; i < players.length; i++) {
        if(player.location == enemy.location) {
            tempClients.push(clients[i]);
        }
    }

    switch(enemy.direction){
        case 1:
        enemy.y = enemy.y + enemy.movementSpeed;
        break;
        case 2:
        enemy.y = enemy.y - enemy.movementSpeed;
        break;
        case 3:
        enemy.x = enemy.x - enemy.movementSpeed;
        break;
        case 4:
        enemy.x = enemy.x + enemy.movementSpeed;
        break;
    }

    if(enemy.x >= 1800 - 100) { enemy.x = 1800 - 110 }
    if(enemy.x <= 0) { enemy.x = 10 }
    if(enemy.y >= 720 - 100) { enemy.y = 720 - 110 }
    if(enemy.y <= 0) { enemy.y = 10 }

    io.sockets.in(enemy.location).emit('moveEnemy', enemy);
}
function moveEnemyCombat(enemy) {

    if(enemy.x > enemy.target.x) { enemy.x = enemy.x - enemy.combatMovementSpeed }
    if(enemy.x < enemy.target.x) { enemy.x = enemy.x + enemy.combatMovementSpeed }

    if(enemy.y > enemy.target.y) { enemy.y = enemy.y - enemy.combatMovementSpeed }
    if(enemy.y < enemy.target.y) { enemy.y = enemy.y + enemy.combatMovementSpeed }

    if(enemy.x >= 1920 - 100) { enemy.x = 1920 - 110 }
    else if(enemy.x <= 0) { enemy.x = 10 }
    if(enemy.y >= 1080 - 100) { enemy.y = 1080 - 110 }
    else if(enemy.y <= 0) { enemy.y = 10 }

    io.sockets.in(enemy.location).emit('moveEnemy', enemy);
}
function moveEnemies(){
    for(var i = 0; i < enemies.length; i++){
        if(enemies[i].alive) {
            if(enemies[i].isInCombat 
                && enemies[i].target != null) {
                    moveEnemyCombat(enemies[i]);
            }
            else {
                moveEnemy(enemies[i]);
            }
        }
    }
}
function changeEnemiesDirection() {
    for(var i = 0; i < enemies.length; i++) {
        var change = Math.floor(Math.random() * 2) + 1;
        if(change == 1) {
            enemies[i].direction = Math.floor(Math.random() * 5) + 1;
        }
    }
}
function enemyDamaged(socket) {
    socket.on('enemyDamaged', function(data){
        var damage = data.damage;
        var targetID = data.sender;
        var attackingPlayer = null;
        for(var i=0; i < players.length; i++) {
            if(players[i].id == targetID) {
                attackingPlayer = players[i];
                break;
            }
        }
        for(var i=0; i < enemies.length; i++) {
            if(enemies[i].id == data.id) {
                enemies[i].currentHealth -= damage;
                if(enemies[i].target == null
                    && attackingPlayer != null 
                    && attackingPlayer.location == enemies[i].location) {
                    enemies[i].target = attackingPlayer;
                    enemies[i].isInCombat = true;
                    io.sockets.in(enemies[i].location).emit('enemyChoseTarget', enemies[i], targetID);
                }
                else {
                    var changeTarget = Math.floor(Math.random() * 10) + 1;
                    if(changeTarget == 5) {
                        enemies[i].target = attackingPlayer;
                        enemies[i].isInCombat = true;
                        io.sockets.in(enemies[i].location).emit('enemyChoseTarget', enemies[i], targetID);
                    }
                }
                if(enemies[i].currentHealth <= 0) {
                    enemies[i].alive = false;
                }
                socket.in(enemies[i].location).broadcast.emit('enemyDamaged', enemies[i], damage);
                break;
            }
        }
    });
}
function respawnEnemy() {
    for(var i=0; i < enemies.length; i++) {
        if(!enemies[i].alive) {
            var rand = Math.round(Math.random()*2) + 1;
            if(rand == 2) {
                enemies[i].currentHealth = enemies[i].maxHealth;
                enemies[i].alive = true;
                io.sockets.in(enemies[i].location).emit('respawnEnemy', enemies[i]);
            }
        }
    }
}
function changeRoom(socket, data){
    socket.leave(socket.room);
    socket.join(data.location);
    for(var i = 0; i < players.length; i++) {
        if(players[i].id == data.id) {
            removeEnemyTarget(players[i]);
            players[i].location = data.location;
            socket.broadcast.emit('playerChangedLocation', players[i]);
        }
    }
}
function removeEnemyTarget(player) {
    for(var i = 0; i < enemies.length; i++) {
        if(enemies[i].target != null && 
            enemies[i].target.id == player.id) {
            enemies[i].target = null;
            enemies[i].isInCombat = false;
            io.sockets.in(enemies[i].location).emit('setEnemyCombatFalse', enemies[i]);
        }
    }
}